//Server Address for buttons.py
var buttonServer = "http://192.168.1.166:8770";

//Key Binds
var bStarter = "e";
var bElectic = "w";
var bParking = "q";
var bWipersOn = "a";
var bWipersOff = "r";
var bBeacon = "o";
var bHighBeams = "b";
var bLights = "l";
var bTrailer = "t";
var bDashMode = "d";
var bMapMode = "s";
var bTruckLiftAxle = "u";
var bTrailerLiftAxle = "i";
var bHazards = "f";
var bDifferentialLock = "v";

//Key Binds for ETS2 Local Radio
var bRadioNext = ",";
var bRadioPrevious = ".";
var bRadioGoToFavorite = "x";
var bRadioMakeFavorite = "z";
var bRadioStop = "[";
var bRadioVolumeUp = "c";
var bRadionVolumeDown = "-";

//Other
var Currency = "&euro;";

//----------------------------------------------------------------------------------------------------

var pullTelemetryReturn;
var truckMake;
var truckModel;
var isEvent = false;
var airPercentage;

function Round(num) {
    if (num.toString().length == 1) {
        return "0";
    } else {
        return num.toString().substring(2, 4);
    }
}

function mtokm(x) {
    if (x == "0") {
        return "0m";
    } else if (x > 999) {
        return Math.round(x / 1000) + "km";
    } else if (x <= 998) {
        return x + "m";
    }
}

function timeThing(estTime) {
    var estTimeDays = estTime.substring(8, 10);
    var estTimeHours = estTime.substring(11, 13);
    var sum;
    if (estTimeDays > 1) {
        var x = (24 * estTimeDays);
        sum = parseInt(x) + parseInt(estTimeHours) - 24;
    } else {
        sum = estTimeHours;
    }
    return sum;
}

function hhmm(data) {
    return data.substring(10).substring(1, 6);
}

function gameDate(data) {
    return data.substring(5, 10);
}

function RealTime() {
    var dt = new Date();
    return dt.toLocaleTimeString();
}

function UpdateButton(id, data, color1, color2 ) {
    if (data == true) { // ON
        $(id).removeClass(color2);
        $(id).addClass(color1);
    } else { // OFF
        $(id).removeClass(color1);
        $(id).addClass(color2);
    }
}

function getTelemetry(CacheHeaders) {
    reqwest({
        method: "GET",
        url: "/api/ets2/telemetry",
        headers: { "Cache-Control":  CacheHeaders },
        crossOrigin: true,
        type: "json",
        success: function(api) { pullTelemetryReturn = api; },
        error: function(error) { UpdateText(".ets2-events-p", "ERROR " + error); pullTelemetryReturn = error; }
    });
    return pullTelemetryReturn;
}

function sendButton(key) {
    xhttp = new XMLHttpRequest();
    xhttp.open("POST", buttonServer, true);
    xhttp.send(key); 
}

function buttonPress(id, key) {
    $("#" + id).click(function() {
        sendButton(key);
    });
}

function UpdateText(element, data) {
    $(element).empty().append(data);
}

$(document).ready(function() {
    fastRefresh = setInterval(function() {
        const api = getTelemetry("no-cache, no-store, must-revalidate");
        if (api.game.connected == false) {
            $(".splash").show();
            $(".dash").hide();
            UpdateText(".ets2-paused", "Not Connected");
        } else if (api.game.paused == true) {
            $(".splash").show();
            $(".dash").hide();
            UpdateText(".ets2-paused", "Paused");
        } else {
            $(".splash").hide();
            $(".dash").show();
        }

        //Update Buttons
        UpdateButton("#ets2-engine-elec", api.truck.electricOn, "button-green", "button-red");
        UpdateButton("#ets2-start-stop", api.truck.engineOn, "button-green", "button-red");
        UpdateButton("#ets2-parking", api.truck.parkBrakeOn, "button-chartreuse", "button-darkgoldenrod");
        UpdateButton("#ets2-beacon", api.truck.lightsBeaconOn, "button-orange", "button-red");
        UpdateButton("#ets2-highlights", api.truck.lightsBeamHighOn, "button-blue", "button-red");
        UpdateButton("#ets2-wiperson", api.truck.wipersOn, "button-yellowgreen", "button-red");
        UpdateButton("#ets2-wipersoff", api.truck.wipersOn, "button-yellowgreen", "button-red");
        if (api.truck.lightsBeamLowOn == true) {
            $("#ets2-lights").removeClass("button-red");
            $("#ets2-lights").removeClass("button-yellow");
            $("#ets2-lights").addClass("button-steelblue");
        } else if (api.truck.lightsParkingOn == true) {
            $("#ets2-lights").removeClass("button-red");
            $("#ets2-lights").removeClass("button-steelblue");
            $("#ets2-lights").addClass("button-yellow");
        } else {
            $("#ets2-lights").removeClass("button-steelblue");
            $("#ets2-lights").removeClass("button-yellow");
            $("#ets2-lights").addClass("button-red");
        }
        //Events
        if (api.finedEvent.fined == true) {
            isEvent = true;
            UpdateText(".ets2-events-p", "FINED " + Currency + api.finedEvent.fineAmount + " (" + api.finedEvent.fineOffense + ")" );
        }
        if (api.tollgateEvent.tollgateUsed == true) {
            isEvent = true;
            UpdateText(".ets2-events-p", "TOLL " + Currency + api.tollgateEvent.payAmount);
        }
        if (api.ferryEvent.ferryUsed == true) {
            isEvent = true;
            UpdateText(".ets2-events-p", "FERRY " + Currency + api.ferryEvent.payAmount + " - " + api.ferryEvent.sourceName + " > " + api.ferryEvent.targetName);
        }
        if (api.trainEvent.trainUsed == true) {
            isEvent = true;
            UpdateText(".ets2-events-p", "TRAIN " + Currency + api.trainEvent.payAmount + " - " + api.trainEvent.sourceName + " > " + api.trainEvent.targetName);
        }
    }, 100);
    mainRefresh = setInterval(function() {
        const api = getTelemetry("no-cache, no-store, must-revalidate");
        truckMake = api.truck.make;
        truckModel = api.truck.model;

        //Air and Fuel
        var fuelPercentage = ((parseInt(api.truck.fuel) / parseInt(api.truck.fuelCapacity)) * 100);
        $(".ets2-truck-fuelvalue").css("height", fuelPercentage + "%");
        if (api.truck.fuelWarningOn == true) {
            $(".ets2-truck-fuelvalue").css({"background-color":"red", "color":"red"});
        } else {
            $(".ets2-truck-fuelvalue").css({"background-color":"", "color":""});
        }
        if (api.truck.airPressure > 100) {
            airPercentage = 100;
        } else {
            airPercentage = api.truck.airPressure;
        }
        $(".ets2-truck-airvalue").css("height", airPercentage + "%");
        if (api.truck.airPressureEmergencyOn == true) {
            $(".ets2-truck-airvalue").css({"background-color":"red", "color":"red"});
        } else if (api.truck.airPressureWarningOn == true) {
            $(".ets2-truck-airvalue").css({"background-color":"orange", "color":"orange"});
        } else {
            $(".ets2-truck-airvalue").css({"background-color":"", "color":""});
        }
    
        UpdateText("title", "vDash - " + api.game.gameName + " - v" + api.game.version);
        UpdateText(".ets2-truck-license", "[" + api.truck.licensePlate + "] " + api.truck.licensePlateCountry);
        if (api.trailer1.attached == true) {
            UpdateText(".ets2-trailer-plate", "[" + api.trailer1.licensePlate + "] " + api.trailer1.licensePlateCountry);
        } else {
            UpdateText(".ets2-trailer-plate", "NO TRAILER");
        }
        UpdateText(".ets2-game-nextRestStopTime", "Rest " + api.game.nextRestStopTime.substring(10).substring(1, 6) + " hrs");
        UpdateText(".ets2-game-time", RealTime() +" - " + hhmm(api.game.time) + " (" + gameDate(api.game.time) + ")");
        UpdateText(".ets2-truck-odometer", Math.round(api.truck.odometer) + "km");

        //Job
        UpdateText(".ets2-job-deadlineTime", "Deadline " + hhmm(api.job.deadlineTime) + " (" + gameDate(api.job.deadlineTime) + ")");
        UpdateText(".ets2-job-remainingTime", "Remaining " + timeThing(api.job.remainingTime) + ":" + api.job.remainingTime.substring(14, 16) + " hrs");
        UpdateText(".ets2-job-income", "" + Currency + api.job.income);
        UpdateText(".ets2-navigation-estimatedTime", "Est. " + timeThing(api.navigation.estimatedTime) + ":" + api.navigation.estimatedTime.substring(14, 16) + ":" + api.navigation.estimatedTime.substring(17, 19) + " hrs");
        UpdateText(".ets2-navigation-estimatedDistance", "Dist " + mtokm(api.navigation.estimatedDistance));
        if (api.job.income > 0) {
            UpdateText(".ets2-cargo-cargo", api.cargo.cargo + " | " + api.job.sourceCompany + " -> " + api.job.destinationCompany);
        } else {
            UpdateText(".ets2-cargo-cargo", "NO CARGO");
        }
    
        //Diagnostics
        //Truck
        UpdateText(".ets2-truck-wearEngine", "Engine " + Round(api.truck.wearEngine) + "%");
        UpdateText(".ets2-truck-wearTransmission", "Transmission " + Round(api.truck.wearTransmission) + "%");
        UpdateText(".ets2-truck-wearCabin", "Cabin " + Round(api.truck.wearCabin) + "%");
        UpdateText(".ets2-truck-wearChassis", "Chassis " + Round(api.truck.wearChassis) + "%");
        UpdateText(".ets2-truck-wearWheels", "Wheels " + Round(api.truck.wearWheels) + "%");
        if (api.trailer2.attached == true) { //Trailer 1 and 2
            UpdateText(".ets2-trailer1-wearWheels", "Trailers Wheels " + Round(api.trailer1.wearWheels) + "% " + Round(api.trailer2.wearWheels) + "%");
            UpdateText(".ets2-trailer1-wearChassis", "Trailers Chassis " + Round(api.trailer1.wearChassis) + "% " + Round(api.trailer2.wearChassis) + "%");
            UpdateText(".ets2-trailer1-wearCargo", "Cargo Damage " + Round(api.trailer1.cargoDamage) + "% " + Round(api.trailer2.cargoDamage) + "%");
        } else { //Trailer 1
            UpdateText(".ets2-trailer1-wearWheels", "Trailer Wheels " + Round(api.trailer1.wearWheels) + "%");
            UpdateText(".ets2-trailer1-wearChassis", "Trailer Chassis " + Round(api.trailer1.wearChassis) + "%");
            UpdateText(".ets2-trailer1-wearCargo", "Cargo Damage " + Round(api.trailer1.cargoDamage) + "%");
        }
    }, 250);
    eventClear = setInterval(function() {
        if (isEvent == true) {
            isEvent = false;
        } else {
            UpdateText(".ets2-events-p", truckMake + "&nbsp;" + truckModel);
        }
    }, 5000);

    //Button Presses
    buttonPress("ets2-start-stop", bStarter);
    buttonPress("ets2-engine-elec", bElectic);
    buttonPress("ets2-parking", bParking);
    buttonPress("ets2-wiperson", bWipersOn);
    buttonPress("ets2-wipersoff", bWipersOff);
    buttonPress("ets2-beacon", bBeacon);
    buttonPress("ets2-highlights", bHighBeams);
    buttonPress("ets2-lights", bLights);
    buttonPress("ets2-trailer", bTrailer);
    buttonPress("ets2-dash", bDashMode);
    buttonPress("ets2-map", bMapMode);
    buttonPress("ets2-next", bRadioNext);
    buttonPress("ets2-previous", bRadioPrevious);
    buttonPress("ets2-gotofav", bRadioGoToFavorite);
    buttonPress("ets2-makefav", bRadioMakeFavorite);
    buttonPress("ets2-stop", bRadioStop);
    buttonPress("ets2-volup", bRadioVolumeUp);
    buttonPress("ets2-voldown", bRadionVolumeDown);

    //Simulate Button Telemetry
    var truckLiftSet = false;
    $("#ets2-trucklift").click(function() {
        if (truckLiftSet == false) {
            truckLiftSet = true;
            $("#ets2-trucklift").addClass("button-orangered");
        } else {
            truckLiftSet = false;
            $("#ets2-trucklift").removeClass("button-orangered");
        }
        sendButton(bTruckLiftAxle);
    });
    var trailerLiftSet = false;
    $("#ets2-trailerlift").click(function() {
        if (trailerLiftSet == false) {
            trailerLiftSet = true;
            $("#ets2-trailerlift").addClass("button-orangered");
        } else {
            trailerLiftSet = false;
            $("#ets2-trailerlift").removeClass("button-orangered");
        }
        sendButton(bTrailerLiftAxle);
    });
    var hazardSet = false;
    $("#ets2-hazard").click(function() {
        if (hazardSet == false) {
            hazardSet = true;
            $("#ets2-hazard").addClass("button-lightgreen");
        } else {
            hazardSet = false;
            $("#ets2-hazard").removeClass("button-lightgreen");
        }
        sendButton(bHazards);
    });
    var diffSet = false;
    $("#ets2-dif").click(function() {
        if (diffSet == false) {
            diffSet = true;
            $("#ets2-dif").addClass("button-orangered");
        } else {
            diffSet = false;
            $("#ets2-dif").removeClass("button-orangered");
        }
        sendButton(bDifferentialLock);
    });
});
