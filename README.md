# [vDash](https://gitlab.com/dowvd1000/vdash)

Copyright (c) 2022 [Dow Van Dine](https://dow.vandine.xyz)  
See [LISENSE](https://gitlab.com/dowvd1000/vdash/-/blob/main/LICENSE)

Virtual Dashboard for ETS2

![Screenshot](screenshot.png)

Uses [https://github.com/mike-koch/ets2-telemetry-server](https://github.com/mike-koch/ets2-telemetry-server)

**License**

You are free to copy, modify, and distribute vdash with attribution under the terms of the 2-Clause BSD license.  
See the [LICENSE](LICENSE) file for details.
