from http.server import HTTPServer, BaseHTTPRequestHandler
from pynput.keyboard import Controller


server_ip = "0.0.0.0"
server_port = 25556
keyboard = Controller()

class Handler(BaseHTTPRequestHandler):
    def do_OPTIONS(self):
        self.send_response(200, "ok")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
        self.send_header("Access-Control-Allow-Headers", "Content-Type")
        self.end_headers()
    def do_POST(self):
        content_length = int(self.headers['Content-Length'])        
        body = self.rfile.read(content_length)
        idata = (str(body)[2:-1])
        keyboard.press(idata)
        print(" > Key Pressed:", idata)
        keyboard.release(idata)

try:
    print(" + Starting Server")
    httpd = HTTPServer((server_ip, server_port), Handler)
    print(" + Started Server")
    httpd.serve_forever()
except KeyboardInterrupt:
    print(" - Stopping Server")
    httpd.server_close()
    print(" - Stopped Server")
